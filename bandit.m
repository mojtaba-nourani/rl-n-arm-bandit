% Mojtaba Nourani; 9523405064; 2016
clc;clear;t1=clock;close all;
%% Initialization:
CycleNumbers = 20; %number of Cycles
Q0=5;% optimistic initial value
KH=1; Tav=.01;   % for Softmax action selection
H=menu('Choose an action selection method:','Greedy+eps','Optimistic+Realistic','Softmax');
if H==1
    Epsilon = [0,0.01,0.1];     % 0 for pure greedy, 0.01-greedy, 0.1-greedy
elseif H==2
    Epsilon = [0,0.01,0.1];     % 0 for Optimistic greedy, 0.01 for realistic 0.01-greedy
elseif H==3
    Epsilon = [0,0,0.1];KH=0;     % 0 for pure greedy, 0.01-greedy, 0.1-greedy
end

Allmu=randn(10,length(Epsilon)*CycleNumbers);
for eps_selection = 1:length(Epsilon)     % Selecting Strategy
    epsilon=Epsilon(eps_selection);
    TotalRewards = 0;
    optimal_action = 0;
    for j=1:CycleNumbers
        %% Environment:
        jj=(eps_selection-1)*CycleNumbers+j;
        mu = Allmu(:,jj);   % These are unknown action values Q*
        [q_star,i_star] = max(mu); % Best action
        rr = randn(10,1000)+mu*ones(1,1000);    %random_reward with mean mu and var=1
        Actual_reward = []; %actual reward for one cycle (=1000 play)
        OneCycleOptimalAction = []; %optimal action for one cycle
        rand_eps = rand(1,1000);    % non-negative uniformally distributed random numbers between 0,1
        %% Agent:
        if H==1 || H==3
            q = zeros(10,1);       % action value estimation with 0 initial
        elseif H==2
            if  eps_selection==0
                q = zeros(10,1);
            else
                q = zeros(10,1)+Q0;% action value estimation with optimistic initial
            end
        end
        k = zeros(10,1);        % Counter for each 10-Arm
        for i = 1:1000
            if H==3 && KH==0
                qq=exp(q/Tav)/sum(exp(q/Tav));
                sss=rand;
                qqq=qq(1);
                softmax=1;
                for kk=1:10
                    if qqq>=sss && softmax
                        im=kk;
                        softmax=0;
                    else
                        if kk~=10
                            qqq=qqq+qq(kk+1);
                        end
                    end
                end
            else
                [qm,im] = max(q);
                if rand_eps(i)<epsilon % not feasible for greedy
                    im = ceil(10*rand_eps(i)/epsilon);  % rand_eps(i)<epsilon leads to (rand_eps(i)<epsilon)<1
                end
            end
            k(im)=k(im)+1;  % Increase Step size of im'th Arm
            r = rr(im,k(im));
            Actual_reward = [Actual_reward r];
            q(im) = q(im) + 1/k(im)*(r-q(im));  % Incremental Implementation of sample average
            %% Environment: Optimal action
            OneCycleOptimalAction = [OneCycleOptimalAction im==i_star];
        end
        %% Results:
        TotalRewards = TotalRewards+Actual_reward;         % calculating sum for generating average
        optimal_action = optimal_action+OneCycleOptimalAction;     % calculating sum for generating average
    end
    
    figure(H);
    subplot(211);hold all;plot(TotalRewards/CycleNumbers);axis([-10 1000 0 1.7]);
    ylabel('Average of Actual Rewards');xlabel('play');grid on
    subplot(212);hold all;plot(100*optimal_action/CycleNumbers);axis([-10 1000 0 100]);
    ylabel('% Average of Optimal Actions');xlabel('play');grid on
    subplot(211);
    
    KH=1;
end
if H==1
    title('Greedy action selection with different eps');
    legend('greedy','0.01-greedy','0.1-greedy');
elseif H==2
    title('Optimistic (Q0=5) & Realistic (Q0=0) greedy');
    legend('Optimistic greedy','Realistic 0.01-greedy','Realistic 0.1-greedy');
elseif H==3
    title('Softmax & greedy & 0.1-greedy');
    legend('Softmax \tau=0.01','greedy','0.1-greedy');
end
t2=clock;
etime(t2,t1)
mean(mean(Allmu,2));
var(var(Allmu,0,2));