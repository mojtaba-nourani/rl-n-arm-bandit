Goal: Comparing action selection methodologies and balancing between exploration and exploitation
Problem definition:
We are faced repeatedly with a choice among n different options, or actions. After each choice you receive a
numerical reward chosen from a stationary probability distribution that depends on the action you selected.